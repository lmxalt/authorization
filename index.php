<?php

include 'connection.php';

session_start();

if(!isset($_SESSION['user_name']) || !isset($_SESSION['id']) ){
   header('location:login.php');
} else {

   if (isset($_POST['submit']) && (isset($_POST['newusername']) || isset($_POST['newnumberph'])
    || isset($_POST['email']) || isset($_POST['newpassword']))) {

      function validate($data){
         $data = trim($data);
         $data = stripslashes($data);
         $data = htmlspecialchars($data);
         return $data;
      }

      //$error = array();
      
      $name = validate(mysqli_real_escape_string($connect, $_POST['newusername']));
      $number = validate($_POST['newnumberph']);
      

      $email = validate(mysqli_real_escape_string($connect, $_POST['newemail']));
      $pass = md5(validate($_POST['newpassword']));
      $cpass = md5(validate($_POST['cpassword']));

   $id = $_SESSION['id'];

   $select = mysqli_query($connect, "SELECT * FROM $table WHERE username = '". $_POST['newusername']."'");
   $select2 = mysqli_query($connect, "SELECT * FROM $table WHERE number = '". $_POST['newnumberph']."'");
   $select3 = mysqli_query($connect, "SELECT * FROM $table WHERE email = '". $_POST['newemail']."'");
   
   $sql = "SELECT * FROM $table WHERE id='$id'";
   $result = mysqli_query($connect, $sql);


   if(mysqli_num_rows($select)) {
      $error[]='Такой никнейм уже существует';
   } 
   else if(mysqli_num_rows($select2)) {
      $error[]='Такой номер уже существует';
   } 
   else if(mysqli_num_rows($select3)) {
      $error[]='Такой email уже существует';
   }

   else if(mysqli_num_rows($result) === 1){
      
      if(!empty($_POST['newnumberph']) and (preg_match("/^[а-яА-ЯЁёa-zA-Z-]*$/iu",$number) or !preg_replace("/[\s\+]+/", "", $number))){  
         $error[] = "На телефоне только цифры и +";
       } else {
      $sql_2 ="UPDATE $table                                                                                                                                                                         
               SET username=IF(LENGTH('$name')=0, username, '$name'), number=IF(LENGTH('$number')=0, number, '$number'), email=IF(LENGTH('$email')=0, email, '$email'), password=IF(LENGTH('$pass')=0, password, '$pass') 
               WHERE id='$id'";
      mysqli_query($connect, $sql_2);
      $row = mysqli_fetch_array($result);
      $_SESSION['user_name'] = $row['username'];
   }
   }
   else {
      //exit();
   
   }
}
}

$title = "Page";
include 'header.php';

?>

<body>
   
<div class="container">

   <div class="content">
      <div class="form-container">
      <form action="" method="post">
      <p>Страница пользователя</p>
      <p>Добро пожаловать <span><?php echo $_SESSION['user_name'] ?></span></p>
      <?php
      if(isset($error)){
         foreach($error as $error){
            echo '<span class="error-msg">'.$error.'</span>';
         };
      };
      ?>
      <input type="text" name="newusername" placeholder="Введите новый логин">
      <input type="numberph" name="newnumberph" placeholder="Введите новый номер">
      <input type="email" name="newemail" placeholder="Введите новый email">
      <input type="password" name="newpassword" placeholder="Введите новый пароль">
      <input type="password" name="cpassword" placeholder="Подтвердите пароль">
      <input type="submit" name="submit" value="Изменить данные" class="btn">

      <a href="login.php" class="btn">Вход</a>
      <a href="register.php" class="btn">Регистрация</a>
      <a href="logout.php" class="btn">Выход</a>
      </form>
      </div>
      
   </div>

</div>

</body>
</html>